# Keeper of the Flame Technical Document
## Table of Contents
  - [Classes](pages/Classes/)
    - [Core](pages/Classes/Core/)
      - [Managers](pages/Classes/Core/Managers/)
        - [Audio Manager](pages/Classes/Core/Managers/AudioManager.md)
        - [Game Manager](pages/Classes/Core/Managers/GameManager.md)
        - [Game UI Manager](pages/Classes/Core/Managers/GameUIManager.md)
        - [Level Manager](pages/Classes/Core/Managers/LevelManager.md)
        - [System UI Manager](pages/Classes/Core/Managers/SystemUIManager.md)
      - [System](pages/Classes/Core/System/)
        - [Enemy Spawn Points](pages/Classes/Core/System/EnemySpawnPoints.md)
        - [Singleton](pages/Classes/Core/System/Singleton.md)
        - [Sound](pages/Classes/Core/System/Sound.md)
    - [Gameplay](pages/Classes/Gameplay/)
      - [Characters](pages/Classes/Gameplay/Characters)
        - [Player](pages/Classes/Gameplay/Characters/Player/)
            - [Animation](pages/Classes/Gameplay/Characters/Player/Animation.md)
            - [Combat](pages/Classes/Gameplay/Characters/Player/Combat.md)
            - [Input](pages/Classes/Gameplay/Characters/Player/Input.md)
            - [Movement](pages/Classes/Gameplay/Characters/Player/Movement.md)
        - [Enemy](pages/Classes/Gameplay/Characters/Enemy/)
          - [Class](pages/Classes/Gameplay/Characters/Enemy/Class.md)
  - [Unity](pages/Unity/)
    - [Prefabs](pages/Unity/Prefabs/)
        - [Characters](pages/Unity/Prefabs/Characters/)
          - [Player](pages/Unity/Prefabs/Characters/Player.md)
          - [Enemy](pages/Unity/Prefabs/Characters/Enemy.md)
        - [Items](pages/Unity/Prefabs/Items/)
          - [Door](pages/Unity/Prefabs/Items/Door.md)
          - [Gem](pages/Unity/Prefabs/Items/Gem.md)
      - [Creating Levels](pages/Unity/CreatingLevels.md)