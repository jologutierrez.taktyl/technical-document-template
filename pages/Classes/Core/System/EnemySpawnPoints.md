# Enemy Spawn Points
## Description
The `EnemySpawnPoints` class is used to contain multiple spawn points per level for easier use with the [`LevelManager`](../Managers/LevelManager.md)
## Snippet
```cs
using UnityEngine;

public class EnemySpawnPoints : MonoBehaviour
{
    public Transform[] points;
}
```