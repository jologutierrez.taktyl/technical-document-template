# Game UI Manager
## Table of Contents
  - [Description](#description)
  - [Variables](#variables)
  - [Methods](#methods)
  - [Snippet](#snippet)
## Description
The `GameUIManager` class handles the in-game UI such as the HUD.
## Variables
| Variable                            | Description                                                                                                                                                                    |
| ----------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `CanvasGroup endScreenCanvas`       | [`CanvasGroup`](https://docs.unity3d.com/ScriptReference/CanvasGroup.html) used to transition to the end screen                                                                |
| `GameObject instructionsCanvas`     | GameObject of the canvas that displays the game instructions in-world                                                                                                          |
| `bool firstRun`                     | `True` when it is only the first run. Used to determine whether instructions should be displayed                                                                               |
| `GameObject playerCanvas`           | GameObject of the canvas that displays the HUD                                                                                                                                 |
| `TextMeshProUGUI playerLivesCount`  | [`TextMeshProUGUI`](https://docs.unity3d.com/Packages/com.unity.textmeshpro@1.1/api/TMPro.TextMeshProUGUI.html) that displays the player's remaining lives                     |
| `TextMeshProUGUI timerCountDisplay` | [`TextMeshProUGUI`](https://docs.unity3d.com/Packages/com.unity.textmeshpro@1.1/api/TMPro.TextMeshProUGUI.html) that displays the round time                                   |
| `GameObject GameEndCanvas`          | GameObject of the canvas that displays the end screen                                                                                                                          |
| `TextMeshProUGUI GameEndText`       | [`TextMeshProUGUI`](https://docs.unity3d.com/Packages/com.unity.textmeshpro@1.1/api/TMPro.TextMeshProUGUI.html) that displays whether the player won or lost                   |
| `TextMeshProUGUI TimerDisplay`      | [`TextMeshProUGUI`](https://docs.unity3d.com/Packages/com.unity.textmeshpro@1.1/api/TMPro.TextMeshProUGUI.html) that displays the time it took for the player to end the level |
| `PlayerCombat _playerCombat`        | [`PlayerCombat`](Combat.md) component of the player                                                                                                                            |
## Methods
| Method                   | Description                                       |
| ------------------------ | ------------------------------------------------- |
| `void GameOverRoutine()` | Hides the HUD and starts showing the end screen   |
| `void GameOver()`        | Sets the correct text and time for the end screen |
| `IEnumerator FadeIn()`   | Fade in animation for `endScreenCanvas`           |
## Snippet
```cs
using System.Collections;
using UnityEngine;
using TMPro;

public class GameUIManager : MonoBehaviour
{
    [Header("Transitions")]
    [SerializeField] private CanvasGroup endScreenCanvas;

    [Header("In Game Instructions")]
    [SerializeField] private GameObject instructionsCanvas;
    [SerializeField] private bool firstRun = true;

    [Header("In Game Display")]
    [SerializeField] private GameObject playerCanvas;
    [SerializeField] private TextMeshProUGUI playerLivesCount;
    [SerializeField] private TextMeshProUGUI timerCountDisplay;
    
    [Header("End Game Display")]
    [SerializeField] private GameObject GameEndCanvas;
    [SerializeField] private TextMeshProUGUI GameEndText;
    [SerializeField] private TextMeshProUGUI TimerDisplay;
    
    private PlayerCombat _playerCombat;

    private void OnEnable()
    {
        GameManager.Instance.OnGameStart += () =>
        {
            playerCanvas.SetActive(true);
            
            if(firstRun) 
            { instructionsCanvas.SetActive(true); }
            
            GameEndCanvas.SetActive(false);
        };

        GameManager.Instance.OnGameEnd += GameOverRoutine;
    }

    private void GameOverRoutine()
    {
        GameEndCanvas.SetActive(true);
        playerCanvas.SetActive(false);
        instructionsCanvas.SetActive(false);
        endScreenCanvas.alpha = 0;
        GameOver();
        StartCoroutine(FadeIn());
    }

    private void Start()
    {
        playerCanvas.SetActive(false);
        instructionsCanvas.SetActive(false);
        GameEndCanvas.SetActive(false);
        AudioManager.Instance.Play("bgm");
    }

    private void Update()
    {
        if (!GameManager.Instance.IsGameStart) return;
        _playerCombat = FindObjectOfType<PlayerCombat>();
        playerLivesCount.text = $"x {_playerCombat.Health}";
        timerCountDisplay.text = $"{(int) GameManager.Instance.Span.TotalMinutes}:{GameManager.Instance.Span.Seconds:00}";
        if (FindObjectOfType<PlayerMovement>().HasGem)
            instructionsCanvas.SetActive(false);
    }

    private void GameOver()
    {
        firstRun = false;
        if (GameManager.Instance.playerWon)
        {
            GameEndText.text = "Congratulations";
            GameEndText.color = Color.yellow;
        }
        else
        {
            GameEndText.text = "Game Over";
            GameEndText.color = Color.red;
        }
        TimerDisplay.text = $"You ran for {timerCountDisplay.text}";
    }

    private IEnumerator FadeIn()
    {
        while (endScreenCanvas.alpha != 1)
        {
            endScreenCanvas.alpha += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

}
```