# Level Manager
## Table of Contents
  - [Description](#description)
  - [Variables](#variables)
  - [Methods](#methods)
  - [Snippet](#snippet)
## Description
The `LevelManager` class handles the loading of levels, tracking of the camera, and spawning of gameobjects.
## Variables
| Variable                              | Description                                                                                                                                                                     |
| ------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `GameObject[] levels`                 | Walkable levels that are changed in-game                                                                                                                                        |
| `GameObject playerPrefab`             | Prefab of the player                                                                                                                                                            |
| `Transform playerSpawn`               | Place where the player is spawned on the level                                                                                                                                  |
| `CinemachineVirtualCamera vCam`       | Reference to a [`CinemachineVirtualCamera`](https://docs.unity3d.com/Packages/com.unity.cinemachine@2.8/api/Cinemachine.CinemachineVirtualCamera.html) which follows the player |
| `GameObject _player`                  | Runtime instance of the player                                                                                                                                                  |
| `GameObject[] enemyPrefabs`           | Array of enemy prefabs that can be spawned in the level                                                                                                                         |
| `EnemySpawnPoints[] enemySpawnPoints` | Place where the enemies can be spawned in the levels                                                                                                                            |
| `List<GameObject> _enemies`           | Runtime instance of the enemies                                                                                                                                                 |
| `GameObject gemPrefab`                | Prefab of the gem                                                                                                                                                               |
| `Transform gemSpawnPoint`             | Place where the gem is spawned in the level                                                                                                                                     |
| `GameObject _gem`                     | Runtime instance of the gem                                                                                                                                                     |
| `GameObject doorPrefab`               | Prefab of the door                                                                                                                                                              |
| `GameObject _door`                    | Runtime instance of the door                                                                                                                                                    |
## Methods
| Method                         | Description                                           |
| ------------------------------ | ----------------------------------------------------- |
| `void LoadLevel()`             | Loads the level                                       |
| `void UnloadLevel()`           | Clears the level                                      |
| `void ChangeLevel()`           | Changes the level based on `levels`                   |
| `void SpawnPlayer()`           | Spawns player and sets up `vCam`                      |
| `void SpawnEnemies(int level)` | Spawns and relocates enemies depending on the `level` |
| `void SpawnGem()`              | Spawns the gem                                        |
| `void SpawnDoor()`             | Spawns the door                                       |
| `void DespawnPlayer()`         | Despawns the player                                   |
| `void DespawnEnemies()`        | Despawns the enemies                                  |
| `void DespawnGem()`            | Despawns the gem                                      |
| `void DespawnDoor()`           | Despawns the door                                     |
## Snippet
```cs
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class LevelManager : MonoBehaviour
{
    [Header("Level Spawner")]
    [SerializeField] private GameObject[] levels;

    [Header("Player Spawn")]
    [SerializeField] private GameObject playerPrefab;
    [SerializeField] private Transform playerSpawn;
    [SerializeField] private CinemachineVirtualCamera vCam;

    private GameObject _player;

    [Header("Enemy Spawn")]
    [SerializeField] private GameObject[] enemyPrefabs;
    [SerializeField] private EnemySpawnPoints[] enemySpawnPoints;
    [SerializeField] private List<GameObject> _enemies = new List<GameObject>();

    [Header("Gem Spawn")]
    [SerializeField] private GameObject gemPrefab;
    [SerializeField] private Transform gemSpawnPoint;
    private GameObject _gem;

    [Header("Exit Spawn")]
    [SerializeField] private GameObject doorPrefab;
    private GameObject _door;
    
    private void OnEnable()
    {
        GameManager.Instance.OnGameStart += LoadLevel;
        //GameManager.Instance.OnGameEnd += UnloadLevel;
    }

    private void OnDisable()
    {
        GameManager.Instance.OnGameStart -= LoadLevel;
        _player.GetComponent<PlayerMovement>().OnGemObtain -= ChangeLevel;
        //GameManager.Instance.OnGameEnd -= UnloadLevel;
    }

    private void LoadLevel()
    {
        // Despawns everything in the game to make sure that there are no duplicates
        UnloadLevel();
        
        levels[0].SetActive(true);
        levels[1].SetActive(false);
        
        SpawnPlayer();
        SpawnEnemies(0);
        SpawnGem();
        
        _player.GetComponent<PlayerMovement>().OnGemObtain += () =>
        {
            SpawnEnemies(1);
            SpawnDoor();
        };
    }

    public void UnloadLevel()
    {
        DespawnPlayer();
        DespawnEnemies();
        DespawnGem();
        DespawnDoor();
    }

    private void ChangeLevel()
    {
        print("Changing Level");
        levels[0].SetActive(false);
        levels[1].SetActive(true);
    }
    
    private void SpawnPlayer()
    {
        _player = Instantiate(playerPrefab, playerSpawn.position, Quaternion.identity);
        _player.GetComponent<PlayerMovement>().OnGemObtain += ChangeLevel;
        vCam.m_Follow = _player.transform;
    }

    private void SpawnEnemies(int level)
    {
        if(level == 0)
            DespawnEnemies();

        var points = enemySpawnPoints[level].points;

        for (int i = 0; i < points.Length; i++)
        {
            _enemies.Add(Instantiate(enemyPrefabs[Random.Range(0, enemyPrefabs.Length)], points[i].position, Quaternion.identity));
        }
        
        print(_enemies.Count + " | " + points.Length );
        
        if (_enemies.Count > points.Length)
        {
            for (int i = 0; i < points.Length; i++)
            {
                if(_enemies[i] != null){
                    _enemies[i].transform.position = points[i].transform.position;
                }
            }
        }
    }

    private void SpawnGem()
    {
        _gem = Instantiate(gemPrefab, gemSpawnPoint.position, Quaternion.identity);
    }

    private void SpawnDoor()
    {
        _door = Instantiate(doorPrefab, playerSpawn.position, Quaternion.identity);
    }
    
    private void DespawnPlayer()
    {
        Destroy(_player);
    }

    private void DespawnEnemies()
    {
        foreach (var enemy in _enemies)
        {
            if (enemy != null)
            {
                Destroy(enemy);
            }
        }
        _enemies.Clear();
    }

    private void DespawnGem()
    {
        Destroy(_gem);
    }

    private void DespawnDoor()
    {
        Destroy(_door);
    }
    
}
```