# Game Manager
## Table of Contents
  - [Description](#description)
  - [Variables](#variables)
    - [Private](#private)
    - [Public](#public)
  - [Methods](#methods)
  - [Snippet](#snippet)
## Description
The `GameManager` class handles the start and end of levels as well as keeping track of the time for each round. Inherits from [`Singleton`](../System/Singleton.md)
## Variables
### Private
| Variable            | Description                                                                                                                          |
| ------------------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| `bool _isGameStart` | Becomes `True` when the game starts                                                                                                  |
| `bool _isGameEnd`   | Becomes `True` when player dies or reaches the exit                                                                                  |
| `float _timer`      | Used to count the time of the round                                                                                                  |
| `TimeSpan _span`    | [`TimeSpan`](https://docs.microsoft.com/en-us/dotnet/api/system.timespan?view=net-6.0) used to convert `_timer` to an `mm:ss` format |
### Public
| Variable                   | Description                                            |
| -------------------------- | ------------------------------------------------------ |
| `bool playerWon`           | `True` if player reaches the exit; `False` if they die |
| `bool IsGameStart`         | public variable of `_isGameStart`                      |
| `bool IsGameEnd`           | public variable of `_isGameEnd`                        |
| `float Timer`              | public variable of `_timer`                            |
| `TimeSpan Span`            | public variable of `_span`                             |
| `event Action OnGameStart` | Event which gets invoked when the game is started      |
| `event Action OnGameEnd`   | Event which gets invoked when the game has ended       |
## Methods
| Method                           | Description                                        |
| -------------------------------- | -------------------------------------------------- |
| `void StartGame()`               | Invokes `OnGameStart` and resets the timer         |
| `void GameEnd(bool isPlayerWin)` | Invokes `OnGameEnd` and sets value for `playerWon` |
## Snippet
```cs
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    private bool _isGameStart;
    private bool _isGameEnd;
    private float _timer;
    private TimeSpan _span;
    
    public bool playerWon;

    public bool IsGameStart => _isGameStart;
    public bool IsGameEnd => _isGameEnd;
    public float Timer => _timer;
    public TimeSpan Span => _span;
    public event Action OnGameStart;
    public event Action OnGameEnd;

    public void StartGame()
    {
        OnGameStart?.Invoke();
        _timer = 0f;
        _isGameStart = true;
        _isGameEnd = false;
    }
    
    private void Update()
    {
        if (_isGameStart)
        {
            _timer += Time.deltaTime;
            _span = TimeSpan.FromSeconds(_timer);
        }
    }

    public void GameEnd(bool isPlayerWin)
    {
        _isGameStart = false;
        _isGameEnd = true;
        playerWon = isPlayerWin;
        OnGameEnd?.Invoke();
    }
}
```