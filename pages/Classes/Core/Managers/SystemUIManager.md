# System UI Manager
## Table of Contents
  - [Description](#description)
  - [Variables](#variables)
  - [Methods](#methods)
  - [Snippet](#snippet)
## Description
The `SystemUIManager` class handles system UI like menus and the instructions page.
## Variables
| Variable                  | Description                                                                                                              |
| ------------------------- | ------------------------------------------------------------------------------------------------------------------------ |
| Animator transitions      | [`Animator`](https://docs.unity3d.com/ScriptReference/Animator.html) of an image for transitions found in the game scene |
| GameObject mainMenuUI     | GameObject of the canvas that displays the main menu                                                                     |
| GameObject instructionsUI | GameObject of the canvas that displays the instructions                                                                  |
| GameObject gameEndUI      | GameObject of the canvas that displays the end screen                                                                    |
## Methods
| Method                           | Description                                                                                                                        |
| -------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------- |
| void OnGameStart()               | Hides the main menu when game starts                                                                                               |
| void StartGame()                 | Used by a [`Button`](https://docs.unity3d.com/ScriptReference/UIElements.Button.html) to start the game when clicked               |
| IEnumerator ToggleGame()         | Plays transition and starts the game                                                                                               |
| void ShowInstructions            | Used by a [`Button`](https://docs.unity3d.com/ScriptReference/UIElements.Button.html) to show instructions when clicked            |
| IEnumerator ToggleInstructions() | Plays transition and whows instructions page                                                                                       |
| void ReturnToMainMenu()          | Used by a [`Button`](https://docs.unity3d.com/ScriptReference/UIElements.Button.html) to show return to main menu when clicked     |
| IEnumerator ToggleMainMenu()     | Plays transition and shows main menu                                                                                               |
| void ExitGame()                  | Used by a [`Button`](https://docs.unity3d.com/ScriptReference/UIElements.Button.html) to show return to exit the game when clicked |
| IEnumerator TransitionFade()     | Fade animation for `transitions`                                                                                                   |
## Snippet
```cs
using System.Collections;
using UnityEngine;
using TMPro;

public class SystemUIManager : MonoBehaviour
{
    [Header("Transitions")]
    [SerializeField] private Animator transitions;
    
    [Header("Game Canvas")]
    [SerializeField] private GameObject mainMenuUI;
    [SerializeField] private GameObject instructionsUI;
    [SerializeField] private GameObject gameEndUI;

    private void OnEnable()
    {
        GameManager.Instance.OnGameStart += OnGameStart;
    }

    private void OnDisable()
    {
        GameManager.Instance.OnGameStart -= OnGameStart;
    }


    private void Start()
    {
        mainMenuUI.SetActive(true);
        instructionsUI.SetActive(false);
    }

    private void OnGameStart()
    {
        mainMenuUI.SetActive(false);
    }

    public void StartGame()
    {
        StartCoroutine(ToggleGame());
    }

    private IEnumerator ToggleGame()
    {
        AudioManager.Instance.Play("Click");
        StartCoroutine(TransitionFade());
        yield return new WaitForSeconds(1f);
        GameManager.Instance.StartGame();
    }

    public void ShowInstructions()
    {
        StartCoroutine(ToggleInstructions());
    }

    private IEnumerator ToggleInstructions()
    {
        AudioManager.Instance.Play("Click");
        StartCoroutine(TransitionFade());
        yield return new WaitForSeconds(1f);
        mainMenuUI.SetActive(false);
        instructionsUI.SetActive(true);
    }

    public void ReturnToMainMenu()
    {
        StartCoroutine(ToggleMainMenu());
    }

    private IEnumerator ToggleMainMenu()
    {
        AudioManager.Instance.Play("Click");
        StartCoroutine(TransitionFade());
        yield return new WaitForSeconds(1f);
        FindObjectOfType<LevelManager>().UnloadLevel();
        mainMenuUI.SetActive(true);
        instructionsUI.SetActive(false);
        gameEndUI.SetActive(false);
    }

    public void ExitGame()
    {
        AudioManager.Instance.Play("Click");
        Application.Quit();
    }

    private IEnumerator TransitionFade()
    {
        transitions.SetTrigger("FadeOut");
        yield return new WaitForSeconds(1f);
        transitions.SetTrigger("FadeIn");

    }
    
}
```