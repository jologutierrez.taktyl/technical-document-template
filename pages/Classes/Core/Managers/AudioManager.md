# Audio Manager
## Description
The `AudioManager` class is used to play [`Sound`](../System/Sound.md) from a given collection and can be accessed from any class. Inherits from [`Singleton`](../System/Singleton.md)
## Variables
| Variable         | Description                                                                                         |
| ---------------- | --------------------------------------------------------------------------------------------------- |
| `Sound[] sounds` | An array of [`Sound`](../System/Sound.md) which the `AudioManager` uses to verify if a sound exists |
## Methods
| Method                 | Description                                                |
| ---------------------- | ---------------------------------------------------------- |
| void Play(string name) | Checks the `name` from `sounds` and plays if it is present |
| void Play(Sound s)     | Plays `s` without checking `sounds`                        |
| void Stop(string name) | Looks for `name` in `sounds` and stops it from playing     |
## Snippet
```cs
using System;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    public Sound[] sounds;

    protected override void Awake()
    {
        base.Awake();
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found");
            return;
        }
        s.source.Play();
    }

    public void Play(Sound s)
    {
        s.source = gameObject.AddComponent<AudioSource>();
        s.source.clip = s.clip;
        s.source.volume = s.volume;
        s.source.pitch = s.pitch;
        s.source.loop = s.loop;
        s.source.Play();
    }
    
    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found");
            return;
        }
        s.source.Stop();
    }
    
}
```