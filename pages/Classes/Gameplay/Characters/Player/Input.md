# Input
## Table of Contents
  - [Description](#description)
  - [Variables](#variables)
    - [Private](#private)
    - [Public](#public)
  - [Snippet](#snippet)
## Description
The `PlayerInput` class handles the inputs of the player.
## Variables 
### Private
| Variable                   | Description                                                              |
| -------------------------- | ------------------------------------------------------------------------ |
| `PlayerMovement _movement` | [`PlayerMovement`](Movement.md) component of the player                  |
| `PlayerCombat _combat`     | [`PlayerCombat`](Combat.md) component of the player                      |
| `float _direction`         | The direction which the player based on `Input.GetAxisRaw("Horizontal"`) |
| `bool _isFlipped`          | `True` when the player is facing opposite their default direction        |
### Public
| Variable              | Description                                                    |
| --------------------- | -------------------------------------------------------------- |
| `float Direction`     | public variable of `_direction`                                |
| `bool IsFlipped`      | public variable of `_isFlipped`                                |
| `event Action Jump`   | Event which gets invoked whenever the jump button is pressed   |
| `event Action Attack` | Event which gets invoked whenever the attack button is pressed |

## Snippet
```cs
using System;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    private PlayerMovement _movement;
    private PlayerCombat _combat;

    private float _direction;
    private bool _isFlipped;
   
    public float Direction => _direction;
    public bool IsFlipped => _isFlipped;
    
    public event Action Jump;
    public event Action Attack;

    private void Awake()
    {
        _movement = GetComponent<PlayerMovement>();
        _combat = GetComponent<PlayerCombat>();
    }
    
    private void Update()
    {
        // Doesn't return any inputs if player is dead
        if (_combat.IsDead) return;

        // Prevents player from flipping while game isn't playing
        _direction = GameManager.Instance.IsGameEnd ? 0 : Input.GetAxisRaw("Horizontal");

        if (_direction < 0)
        {
            _isFlipped = true;
        }
        // used else if to retain player orientation
        else if (_direction > 0)
        {
            _isFlipped = false;
        }
        
        // Makes sure that Attack can only be invoked if player is able to attack
        if(Input.GetKeyDown(KeyCode.Z) && _combat.CanAttack){Attack?.Invoke();}
        
        // Makes sure that Jump can only be invoked if player is grounded
        if(Input.GetKeyDown(KeyCode.X) && _movement.IsGrounded){Jump?.Invoke();}
    }
}
```
