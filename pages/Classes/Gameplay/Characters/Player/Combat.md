# Combat
## Table of Contents
  - [Description](#description)
  - [Variables](#variables)
    - [Private](#private)
    - [Public](#public)
  - [Methods](#methods)
  - [Snippet](#snippet)
## Description
The `PlayerCombat` class handles the combat system of the player.
## Variables
### Private
| Variable                   | Description                                        |
| -------------------------- | -------------------------------------------------- |
| `float maxHealth`          | The max health of the player                       |
| `float _currentHealth`     | The current health of the player                   |
| `bool _isDead`             | `True` when the player is dead                     |
| `float startTimeBtwAttack` | The maximum time between attacks                   |
| `Transform attackPos`      | The position of where the attack would start       |
| `float attackRange`        | Determines the area of attack from the `attackPos` |
| `LayerMask enemyLayer`     | Determines which layers are the enemy              |
| `float damage`             | The damage amount given by the player              |
| `PlayerInput _input`       | [`PlayerInput`](Input.md) component of the player  |
| `float _timeBtwAttack`     | The time it takes before the next attack           |
| `bool _canAttack`          | `True` when `_timeBtwAttack` is 0                  |
### Public
| Variable                  | Description                                           |
| ------------------------- | ----------------------------------------------------- |
| `float Health`            | public variable for `_currentHealth`                  |
| `bool CanAttack`          | public variable for `_canAttack`                      |
| `bool IsDead`             | public variable for `_isDead`                         |
| `event Action GetDamaged` | Event which gets invoked when the player gets damaged |
| `event Action OnDeath`    | Event which gets invoked when the player dies         |
## Methods
| Method                                | Description                                                                                                                                                      |
| ------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `void Attack()`                       | Uses [`Physics2D.OverlapCircleAll`](https://docs.unity3d.com/ScriptReference/Physics2D.OverlapCircleAll.html) to deal damage to enemies within the `attackRange` |
| `void TakeDamage(float damageAmount)` | Invokes `GetDamaged` action and reduces the health of the player based on the `damageAmount` taken                                                               |
| `IEnumerator DeathRoutine()`          | Invokes `OnDeath` action upon the death of the player                                                                                                            |
## Snippet
```cs
using System;
using System.Collections;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    [Header("Health")]
    [SerializeField] private float maxHealth;
    private float _currentHealth;
    private bool _isDead;
    
    [Header("Attack Info")]
    [SerializeField] private float startTimeBtwAttack;
    [SerializeField] private Transform attackPos;
    [SerializeField] private float attackRange;
    [SerializeField] private LayerMask enemyLayer;
    [SerializeField] private float damage;

    private PlayerInput _input;
    private float _timeBtwAttack;
    private bool _canAttack;

    public float Health => _currentHealth <= 0 ? 0 : _currentHealth;
    public bool CanAttack => _canAttack;
    public bool IsDead => _isDead;

    public event Action GetDamaged;
    public event Action OnDeath;

    private void Awake()
    {
        _input = GetComponent<PlayerInput>();
    }

    private void OnEnable()
    {
        _input.Attack += Attack;
    }

    private void OnDisable()
    {
        _input.Attack -= Attack;
    }

    private void Start()
    {
        _timeBtwAttack = startTimeBtwAttack;
        _currentHealth = maxHealth;
        StartCoroutine(DeathRoutine());
    }
    
    private void Update()
    {
        if (_currentHealth <= 0)
        {
            _isDead = true;
        }
        
        if (_timeBtwAttack <= 0)
        {
            _canAttack = true;
        }
        else
        {
            _canAttack = false;
            _timeBtwAttack -= Time.deltaTime;
        }
    }

    private void Attack()
    {
        Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, enemyLayer);
        foreach (var enemy in enemiesToDamage)
        {
            enemy.GetComponent<Enemy>().TakeDamage(damage);
        }
        _timeBtwAttack = startTimeBtwAttack;
    }

    public void TakeDamage(float damageAmount)
    {
        if (_isDead) return;
        GetDamaged?.Invoke();
        _currentHealth -= damageAmount;
    }

    private IEnumerator DeathRoutine()
    {
        while (!_isDead)
        {
            yield return null;
        }
        OnDeath?.Invoke();
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position,attackRange);
    }
    
}
```