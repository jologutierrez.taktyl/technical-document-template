# Movement
## Table of Contents
  - [Description](#description)
  - [Variables](#variables)
    - [Private](#private)
    - [Public](#public)
  - [Methods](#methods)
  - [Snippet](#snippet)
## Description
The `PlayerMovement` class handles the movement and physics of the player.
## Variables
### Private
| Variable               | Description                                                                                         |
| ---------------------- | --------------------------------------------------------------------------------------------------- |
| `float movementSpeed`  | Determines how fast the player movement is                                                          |
| `float jumpForce`      | Determines how high the player jumps                                                                |
| `PlayerInput _input`   | [`PlayerInput`](Input.md) component of the player                                                   |
| `PlayerCombat _combat` | [`PlayerCombat`](Combat.md) component of the player                                                 |
| `Rigidbody2D _rb2D`    | [`RigidyBody2D`](https://docs.unity3d.com/ScriptReference/Rigidbody2D.html) component of the player |
| `bool _isGrounded`     | Determines whether the player is grounded                                                           |
| `bool _hasGem`         | Determines whether the player has the gem                                                           |
### Public
| Variable                   | Description                                                           |
| -------------------------- | --------------------------------------------------------------------- |
| `Rigidbody2D RB2D`         | public variable of `_rb2D`                                            |
| `bool IsGrounded`          | public variable of `_isGrounded`                                      |
| `bool HasGem`              | public variable of `_hasGem`                                          |
| `event Action OnGemObtain` | Event which gets invoked when [`Gem`](../../Items/Gem.md) is obtained |
## Methods
| Method        | Description                                         |
| ------------- | --------------------------------------------------- |
| `void Jump()` | Adds force to the rigidbody to make the player jump |
## Snippet
```cs
using System;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Movement Values")] 
    [SerializeField] private float movementSpeed;
    [SerializeField] private float jumpForce;
    
    private PlayerInput _input;
    private PlayerCombat _combat;
    private Rigidbody2D _rb2D;
    private bool _isGrounded;
    private bool _hasGem;

    public Rigidbody2D RB2D => _rb2D;
    public bool IsGrounded => _isGrounded;
    public bool HasGem => _hasGem;

    public event Action OnGemObtain;

    private void Awake()
    {
        _input = GetComponent<PlayerInput>();
        _combat = GetComponent<PlayerCombat>();
        _rb2D = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        _input.Jump += Jump;
    }

    private void OnDisable()
    {
        _input.Jump -= Jump;
    }
    
    private void Update()
    {
        transform.rotation = _input.IsFlipped ? Quaternion.Euler(0, 180, 0) : Quaternion.identity;
    }

    private void FixedUpdate()
    {
        _rb2D.velocity = _combat.IsDead ? 
                Vector2.zero : 
                new Vector2(_input.Direction * movementSpeed, _rb2D.velocity.y);
    }

    private void Jump()
    {
        _isGrounded = false;
        _rb2D.velocity = Vector2.up * jumpForce;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("Ground"))
        {
            AudioManager.Instance.Play("Land");
            _isGrounded = true;
        }

        if (other.collider.CompareTag("Gem"))
        {
            AudioManager.Instance.Play("Gem");
            Destroy(other.gameObject);
            _hasGem = true;
            OnGemObtain?.Invoke();
        }

        if (other.collider.CompareTag("Exit"))
        {
            AudioManager.Instance.Play("Exit");
            GameManager.Instance.GameEnd(isPlayerWin:true);
        }
        
    }
    
    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.collider.CompareTag("Ground"))
        {
            _isGrounded = false;
        }
    }
}
```