# Enemy Prefab
## Structure
![](../../../../attachments/Enemy/Enemy%20Prefab.png)
### Hierarchy
![](../../../../attachments/Enemy/Enemy%20Prefab%20Hierarchy.png)
- Light Bandit
  - Eyes
  - AtkPoint
### Components
- Light Bandit
  1. Transform
  2. Sprite Renderer
  3. Animator
  4. Box Collider 2D
  5. Rigidbody 2D
  6. Enemy (script)
- Eyes
  1. Transform
- AtkPoint
  1. Transform
## Gizmos
![](../../../../attachments/Enemy/Enemy%20View%20Gizmos.png)
1. Eyes - Yellow Circle
2. View Range - Green Wire Circle
3. Chase View Range - Green Line
4. Stance View Range - Yellow Line
5. Attack View Range - Red Line

![](../../../../attachments/Enemy/Enemy%20Attack%20Gizmos.png)
1. Attack Point - Red Circle
2. Attack Range - Red Wire Circle
