# Creating Levels
> **NOTE:** [`LevelManager`](../Classes/Core/Managers/LevelManager.md) is hardcoded to only use two levels for now.

## Creating the levels
1. In the Heirarchy, look for the `Grid` gameobject under `== LEVEL ==`
<br>![Step 1](../../attachments/Unity/Creating%20Levels/Creating%20the%20levels/Step%201.gif)
2. Add a child `Rectangular Tilemap` to `Grid` and name it `Level x` where `x` is the number of the level
<br>![Step 2](../../attachments/Unity/Creating%20Levels/Creating%20the%20levels/Step%202.gif)
3. In the inspector, add a `Tilemap Collider 2D` and `Composite Collider 2D` to `Level x`
<br>![Step 3](../../attachments/Unity/Creating%20Levels/Creating%20the%20levels/Step%203.gif)
4. On `Tilemap Collider 2D`, tick the `Used By Composite` box and set the `Body Type` to `Static` in the `Rigidbody2D` component
<br>![Step 4](../../attachments/Unity/Creating%20Levels/Creating%20the%20levels/Step%204.gif)
5. Draw the level using the `Tile Pallete`
<br>![Step 5](../../attachments/Unity/Creating%20Levels/Creating%20the%20levels/Step%205.gif)
6. Set a `Ground` tag on the level. 
<br>![Step 6](../../attachments/Unity/Creating%20Levels/Creating%20the%20levels/Step%206.gif)
7. Repeat steps 2 - 6 for each level needed.

## Setting the placements
> **NOTE:** `Player Spawn` is also where the `Door` spawns so it is important to always keep it accessible.
1. Create an empty gameobject, name it `Player Spawn`, give it a gizmo for visibility, set its position, and place it under `== SPAWN LOCATIONS ==`
<br>![Step 1](../../attachments/Unity/Creating%20Levels/Setting%20the%20placements/Step1.gif)
2. Duplicate `Player Spawn`, name it `Gem Spawn` and change its gizmo and location, and place it under `== SPAWN LOCATIONS ==`
<br>![Step 2](../../attachments/Unity/Creating%20Levels/Setting%20the%20placements/Step2.gif)
3. Create an empty gameobject, name it `Enemy Spawns` and place it under `== SPAWN LOCATIONS ==`
<br>![Step 3.1](../../attachments/Unity/Creating%20Levels/Setting%20the%20placements/Step3.1.gif)
4. Create a child gameobject under `Enemy Spawns` and name it after the intended level of that enemy spawn position. Add an `EnemySpawnPoints` to that child object.
<br>![Step 3.2](../../attachments/Unity/Creating%20Levels/Setting%20the%20placements/Step3.2.gif)
5. Add child gameobjects to the `Level` and set its positions. These are where the enemies will spawn.
<br>![Step 3.3](../../attachments/Unity/Creating%20Levels/Setting%20the%20placements/Step3.3.gif)
6. Go to the parent gameobject and lock its properties in the inspector. Grab the enemy positions and place it in the `` field in the `EnemySpawnPoints`
<br>![Step 3.4](../../attachments/Unity/Creating%20Levels/Setting%20the%20placements/Step3.4.gif)
7. Repeat steps 4-6 for each level made.
> **NOTE:** As of the moment, each level has to have the same amount of spawn locations for the enemies in order to spawn properly.

## Using the LevelManager
1. Under `== MANAGER ==` look for `LevelManager`, and lock its properties in the inspector.
<br>![Step 1](../../attachments/Unity/Creating%20Levels/Using%20the%20LevelManager/Step1.gif)
2. In the `LevelManager`, attach the created levels under the `Levels` field, place the `Player Spawn` gameobject under the `Player Spawn` field, add the `Gem Spawn` gameobject to the `Gem Spawn Point` field, and finally grab the levels under `Enemy Spawn` and drag it to the `EnemySpawnPoints` field.
<br>![Step 2](../../attachments/Unity/Creating%20Levels/Using%20the%20LevelManager/Step2.gif)
3. Enter play mode and it should be able to load all the levels and enemies properly.
<br> ![Step 3](../../attachments/Unity/Creating%20Levels/Using%20the%20LevelManager/Step3.gif)